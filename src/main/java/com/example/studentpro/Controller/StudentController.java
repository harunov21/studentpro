package com.example.studentpro.Controller;

import com.example.studentpro.Dto.LogInDto;
import com.example.studentpro.Dto.RegisterRequestDto;
import com.example.studentpro.Dto.StudentRequestDto;
import com.example.studentpro.Model.Student;
import com.example.studentpro.Service.StudentService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @PostMapping
    public Long Register(@Valid @RequestBody RegisterRequestDto registerRequestDto) {
        return studentService.signIn(registerRequestDto);
    }

    @GetMapping
    public String logIn(@Valid @RequestBody LogInDto logInDto) {
        return studentService.logIn(logInDto);
    }

    @GetMapping("/all")
    public Page<StudentRequestDto> getAllStudents(Pageable pageable) {
        return studentService.getAllStudents(pageable);
    }

    @GetMapping("/{id}")
    public Student findById(@PathVariable Long id) {
        return studentService.findById(id);
    }

    @GetMapping("/spec")
    public List<StudentRequestDto> getStudentsWithSpec(@RequestParam(required = false) String name,
                                             @RequestParam(required = false) String surname,
                                             @RequestParam(required = false) String email,
                                             @RequestParam(required = false) Long id){
        return studentService.findWithSpec(name,surname,email,id);
    }

}



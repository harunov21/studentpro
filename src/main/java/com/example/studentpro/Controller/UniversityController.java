package com.example.studentpro.Controller;

import com.example.studentpro.Dto.StudentRequestDto;
import com.example.studentpro.Dto.TeacherRequestDto;
import com.example.studentpro.Dto.UniversityRequestDto;
import com.example.studentpro.Model.Student;
import com.example.studentpro.Model.University;
import com.example.studentpro.Service.UniversityService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/university")
@RestController
@RequiredArgsConstructor

public class UniversityController {
    private final UniversityService universityService;

    @PostMapping
    public Long save(@Valid @RequestBody UniversityRequestDto universityRequestDto){
        return universityService.save(universityRequestDto);
    }

    @GetMapping("/all")
    public List<University> getAll(){
        return universityService.findAll();

    }

    @GetMapping("/id")
    public University findById(@PathVariable Long id ){
        return universityService.findById(id);
    }

    @GetMapping("/{universityId}")
    public List<StudentRequestDto> getUniversitiesStudent(@PathVariable Long universityId)
    {
        return universityService.findStudentWithSpec(universityId);
    }
    @GetMapping("/spec")
    public List<UniversityRequestDto> getAllWithSpec(@RequestParam(required = false) String name,
                                                     @RequestParam(required = false) String location){
        return universityService.findWithSpec(name,location);



    }

}


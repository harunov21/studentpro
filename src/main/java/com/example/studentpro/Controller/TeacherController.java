package com.example.studentpro.Controller;

import com.example.studentpro.Dto.LogInDto;
import com.example.studentpro.Dto.RegisterRequestDto;
import com.example.studentpro.Dto.TeacherRequestDto;
import com.example.studentpro.Model.Teacher;
import com.example.studentpro.Service.TeacherService;
import jakarta.validation.Valid;
import jdk.dynalink.linker.LinkerServices;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
@RequiredArgsConstructor
public class TeacherController {
    private final TeacherService teacherService;


    @PostMapping
    public Long Register(@Valid @RequestBody RegisterRequestDto registerRequestDto){
        return teacherService.register(registerRequestDto);
    }

    @GetMapping
    public String logIn(@Valid @PathVariable LogInDto logInDto){
        return teacherService.logIn(logInDto);
    }

    @GetMapping("/all")
    public Page<TeacherRequestDto> getAllTeachers (Pageable pageable){
        return teacherService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public Teacher findById(@Valid @PathVariable Long id){
        return teacherService.findById(id);
    }

    @GetMapping("/spec")
    public List<TeacherRequestDto> getTeacherWithSpec(@RequestParam(required = false) String name,
                                                      @RequestParam (required = false) String specialization,
                                                      @RequestParam (required = false) String surname
                                                      ){
        return teacherService.findBySpec(name,specialization,surname);
    }
}

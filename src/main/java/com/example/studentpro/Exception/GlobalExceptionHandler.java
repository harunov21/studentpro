package com.example.studentpro.Exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(StudentNotFoundException.class)
    public Map<String ,String > UserNotFoundException(StudentNotFoundException studentNotFoundException){
        Map<String ,String > error = new HashMap<>();
        error.put("error message" , studentNotFoundException.getLocalizedMessage());
        return error;
    }

    @ExceptionHandler(TeacherNotFoundException.class)
    public Map<String ,String > TeacherNotFoundException (TeacherNotFoundException teacherNotFoundException){
        Map<String ,String> error = new HashMap<>();
        error.put("error message" , teacherNotFoundException.getLocalizedMessage());
        return error;
    }
}

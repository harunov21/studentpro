package com.example.studentpro.Dto;

import com.example.studentpro.Model.Student;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TeacherRequestDto {

    String name;
    String surname;
    String email;
    String password;
    Long universityId;
    List<Student> students;

}

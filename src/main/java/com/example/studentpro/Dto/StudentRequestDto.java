package com.example.studentpro.Dto;

import com.example.studentpro.Model.University;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StudentRequestDto {
    Long id;
    String name;
    String surname;
    String email;
    String password;
    Long universityId;


}

package com.example.studentpro.Dto;

import com.example.studentpro.Model.Teacher;
import com.example.studentpro.Model.University;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterRequestDto {
    @Size(min = 3, message = "Daxil etdiyiniz adın uzunluğu 3dən az ola bilməz!")
    @NotBlank(message = "Xahiş olunur adınızı daxil edin!")
    String name;

    @Size(min = 3, message = "Daxil etdiyiniz soyadın uzunluğu 3dən az ola bilməz!")
    @NotBlank(message = "Xahiş olunur soyadınızı daxil edin!")
    String surname;

    @NotBlank(message = "Xahiş olunur emailinizi daxil edin!")
    String email;

    @Positive(message = "Xahiş olunur müsbət ədəd daxil edin!")
    @Size(min = 3, message = "Şifrə 3 dən az ola bilməz!")
    String password;

    Long universityId;

    String specialization;
   

}
package com.example.studentpro.Dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UniversityRequestDto {

    String name;
    String location;
//    Long studentId;
//    Long teacherId;

}

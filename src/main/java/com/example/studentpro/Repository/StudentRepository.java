package com.example.studentpro.Repository;

import com.example.studentpro.Model.Student;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student,Long> , JpaSpecificationExecutor<Student> {

    @Query("select s from Student s where s.email = :email")
    Optional<Student> findEmail(String email);

    @Query("select s from Student s where ("+
    "(:name is null  or s.name = :name)"+
     " and (:surname is null or s.surname = :surname)"+
     " and (:email is null or s.email = :email)"+
     " and (:id is null or s.id = :id)"+
     ")")
    List<Student> findWithSpec(String name, String surname, String email, Long id);
}

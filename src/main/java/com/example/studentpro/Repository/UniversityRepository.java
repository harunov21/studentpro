package com.example.studentpro.Repository;

import com.example.studentpro.Dto.StudentRequestDto;
import com.example.studentpro.Dto.UniversityRequestDto;
import com.example.studentpro.Model.Student;
import com.example.studentpro.Model.University;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UniversityRepository extends JpaRepository<University,Long> , JpaSpecificationExecutor<University> {

    @Query("select u from University u where (" +
            "(:name is null or u.name=:name)" +
            "and(:location is null or u.location=:location)" +
            ")")
    List<UniversityRequestDto> findWithSpec();
}

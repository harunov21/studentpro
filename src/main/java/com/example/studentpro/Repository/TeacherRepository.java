package com.example.studentpro.Repository;

import com.example.studentpro.Dto.TeacherRequestDto;
import com.example.studentpro.Model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher,Long> , JpaSpecificationExecutor<Teacher> {
    Optional<Teacher> findByEmail(String email);

    @Query("select t from Teacher t where (" +
            "(:name is null or t.name=:name)" +
            "and (:specialization is null or t.specialization=:specialization)" +
            "and (:surname is null or t.surname=:surname)" +
            ")")
    List<TeacherRequestDto> findBySpec(String name, String specialization, String surname);
}

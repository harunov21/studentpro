package com.example.studentpro.Service;

import com.example.studentpro.Dto.LogInDto;
import com.example.studentpro.Dto.RegisterRequestDto;
import com.example.studentpro.Dto.StudentRequestDto;
import com.example.studentpro.Model.Student;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;

public interface StudentService {
    Long signIn(RegisterRequestDto registerRequestDto);

    String logIn(LogInDto logInDto);

    Student findById(Long id);

    List<StudentRequestDto> findWithSpec(String name, String surname, String email, Long id);

    Long addTeacher(Long studentId, Long teacherId);

//    Page<Student> getAll(Pageable pageable);

    Page<StudentRequestDto> getAllStudents(Pageable pageable);
}

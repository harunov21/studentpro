package com.example.studentpro.Service;

import com.example.studentpro.Dto.LogInDto;
import com.example.studentpro.Dto.RegisterRequestDto;
import com.example.studentpro.Dto.TeacherRequestDto;
import com.example.studentpro.Model.Teacher;
import lombok.extern.java.Log;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TeacherService {
    Long register(RegisterRequestDto registerRequestDto);

    String logIn(LogInDto logInDto);

    Page<TeacherRequestDto> getAll(Pageable pageable);

    Teacher findById(Long id);

    List<TeacherRequestDto> findBySpec(String name,String specialization,String surname);
}
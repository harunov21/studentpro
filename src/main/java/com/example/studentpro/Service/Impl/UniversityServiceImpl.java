package com.example.studentpro.Service.Impl;

import com.example.studentpro.Dto.StudentRequestDto;
import com.example.studentpro.Dto.UniversityRequestDto;
import com.example.studentpro.Model.Student;
import com.example.studentpro.Model.Teacher;
import com.example.studentpro.Model.University;
import com.example.studentpro.Repository.UniversityRepository;
import com.example.studentpro.Service.StudentService;
import com.example.studentpro.Service.TeacherService;
import com.example.studentpro.Service.UniversityService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UniversityServiceImpl implements UniversityService {
    private final UniversityRepository universityRepository;
    private final ModelMapper modelMapper;
    private final TeacherService teacherService;
    private  final StudentService studentService;

    @Override
    public Long save(UniversityRequestDto universityRequestDto) {
        modelMapper.map(universityRequestDto, University.class);
//        Student student = studentService.findById(universityRequestDto.getStudentId());
//        Teacher teacher = teacherService.findById(universityRequestDto.getTeacherId());

        List<Student> students = new ArrayList<>();
        List<Teacher> teachers = new ArrayList<>();

//        students.add(student);
//        teachers.add(teacher);

        University university = University.builder()
                .name(universityRequestDto.getName())
                .location(universityRequestDto.getLocation())
//                .students(students)
//                .teachers(teachers)
                .build();

        return universityRepository.save(university).getId();
    }

    @Override
    public List<University> findAll() {
        return universityRepository.findAll();
    }

    @Override
    public University findById(Long id) {
      Optional<University> byId =  universityRepository.findById(id);
      byId.orElseThrow(()-> new RuntimeException("University not found"));
      return byId.get();
    }

    @Override
    public List<StudentRequestDto> findStudentWithSpec(Long universityId) {
        University university = universityRepository.findById(universityId)
                .orElseThrow(()->new RuntimeException("University not found" + universityId));

        List<Student> student = university.getStudents();
        return student.stream()
                .map(student1 -> modelMapper.map(student,StudentRequestDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<UniversityRequestDto> findWithSpec(String name,String location) {
        return universityRepository.findWithSpec();
    }


}

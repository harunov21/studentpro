package com.example.studentpro.Service.Impl;

import com.example.studentpro.Dto.LogInDto;
import com.example.studentpro.Dto.RegisterRequestDto;
import com.example.studentpro.Dto.TeacherRequestDto;
import com.example.studentpro.Model.Student;
import com.example.studentpro.Model.Teacher;
import com.example.studentpro.Model.University;
import com.example.studentpro.Repository.TeacherRepository;
import com.example.studentpro.Repository.UniversityRepository;
import com.example.studentpro.Service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {
    private final TeacherRepository teacherRepository;
    private final ModelMapper modelMapper;
    private final UniversityRepository universityRepository;

    @Override
    public Long register(RegisterRequestDto registerRequestDto) {
        modelMapper.map(registerRequestDto, Teacher.class);
        Optional<University> university = universityRepository.findById(registerRequestDto.getUniversityId());
        Teacher teacher = Teacher.builder()
                .name( registerRequestDto.getName())
                .surname(registerRequestDto.getSurname())
                .email(registerRequestDto   .getEmail())
                .password(registerRequestDto.getPassword())
                .specialization(registerRequestDto.getSpecialization())
                .university(university.get())
                .build();

        teacherRepository.save(teacher);
        return teacher.getId();
    }
    @Override
    public String logIn(LogInDto logInDto) {
        Optional<Teacher> byEmail = teacherRepository.findByEmail(logInDto.getEmail());
        byEmail.ifPresent(teacher -> {
            if (!teacher.getPassword().equals(logInDto.getPassword())){
                throw new RuntimeException("Teacher not found");
            }
        });
        return "Log in Success";
        }
    @Override
    public Page<TeacherRequestDto> getAll(Pageable pageable) {
        Page<Teacher> teacherPage = teacherRepository.findAll(pageable);

        return teacherPage.map(teacher -> {
            TeacherRequestDto teacherRequestDto = new TeacherRequestDto();
            teacherRequestDto.setName(teacher.getName());
            teacherRequestDto.setSurname(teacher.getSurname());
            teacherRequestDto.setEmail(teacher.getEmail());
            teacherRequestDto.setPassword(teacher.getPassword());

            return teacherRequestDto;
        });

    }


    @Override
    public Teacher findById(Long id) {
        Optional<Teacher> byId = teacherRepository.findById(id);
        byId.orElseThrow(() -> new RuntimeException("Teacher not found"));
        return byId.get();
    }

    @Override
    public List<TeacherRequestDto> findBySpec(String name,String specialization,String surname) {
        return teacherRepository.findBySpec(name,specialization,surname);
    }
}


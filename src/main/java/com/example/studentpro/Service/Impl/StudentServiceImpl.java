package com.example.studentpro.Service.Impl;

import com.example.studentpro.Dto.LogInDto;
import com.example.studentpro.Dto.RegisterRequestDto;
import com.example.studentpro.Dto.StudentRequestDto;
import com.example.studentpro.Exception.StudentNotFoundException;
import com.example.studentpro.Exception.TeacherNotFoundException;
import com.example.studentpro.Model.Student;
import com.example.studentpro.Model.Teacher;
import com.example.studentpro.Model.University;
import com.example.studentpro.Repository.StudentRepository;
import com.example.studentpro.Repository.TeacherRepository;
import com.example.studentpro.Repository.UniversityRepository;
import com.example.studentpro.Service.StudentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final ModelMapper modelMapper;
    private final StudentRepository studentRepository;
    private final UniversityRepository universityRepository;
    private final TeacherRepository teacherRepository;

    @Override
    public Long signIn(RegisterRequestDto registerRequestDto) {
        modelMapper.map(registerRequestDto, Student.class);
        Optional<University> university = universityRepository.findById(registerRequestDto.getUniversityId());

        Student student = Student.builder()
                .name(registerRequestDto.getName())
                .surname(registerRequestDto.getSurname())
                .email(registerRequestDto.getEmail())
                .password(registerRequestDto.getPassword())
                .university(university.get())
                .build();
        studentRepository.save(student);

        return student.getId();
    }

    @Override
    public String logIn(LogInDto logInDto) {
       Optional<Student> student =  studentRepository.findEmail(logInDto.getEmail());
       student.ifPresent(student1 -> {
           if (!student1.getPassword().equals(logInDto.getPassword())){
               throw new RuntimeException("User Not Found");
           }
       }); return "Log in success";
    }

    @Cacheable(cacheNames = "customCache",key = "#id")
    @Override
    public Student findById(Long id) {
        Optional<Student> cachedStudent = studentRepository.findById(id);
        if (cachedStudent.isPresent()) {
            return cachedStudent.get();
        } else {
            Student student = studentRepository.findById(id)
                    .orElseThrow(() -> new RuntimeException("User not found"));
            return student;
        }

    }
    @Override
    public List<StudentRequestDto> findWithSpec(String name, String surname, String email, Long id) {
        List<Student> students = studentRepository.findWithSpec(name, surname, email, id);

      return students.stream()
                .map(student -> modelMapper.map(student,StudentRequestDto.class))
                .collect(Collectors.toList());

    }

    @Override
    public Long addTeacher(Long studentId, Long teacherId) {
        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new StudentNotFoundException("Student not found with ID: " + studentId));

        Teacher teacher = teacherRepository.findById(teacherId)
                .orElseThrow(() -> new TeacherNotFoundException("Teacher not found with ID: " + teacherId));

        if (!student.getTeachers().contains(teacher)) {
            student.getTeachers().add(teacher);
            studentRepository.save(student);
        }

        return studentId;
    }

    @Override
    public Page<StudentRequestDto> getAllStudents(Pageable pageable) {
        Page<Student> studentsPage = studentRepository.findAll(pageable);
        return studentsPage.map(student -> {
            StudentRequestDto dto = new StudentRequestDto();
            dto.setId(student.getId());
            dto.setName(student.getName());
            dto.setEmail(student.getEmail());
            dto.setSurname(student.getSurname());
            return dto;
        });
}

}










package com.example.studentpro.Service;

import com.example.studentpro.Dto.StudentRequestDto;
import com.example.studentpro.Dto.UniversityRequestDto;
import com.example.studentpro.Model.Student;
import com.example.studentpro.Model.University;

import java.util.List;

public interface UniversityService {
    Long save(UniversityRequestDto universityRequestDto);

    List<University> findAll();

    University findById(Long id);

    List<StudentRequestDto> findStudentWithSpec(Long universityId);

    List<UniversityRequestDto> findWithSpec(String name,String location);
}

package com.example.studentpro.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class University implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String location;

    @OneToMany(mappedBy = "university" , fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JsonIgnore
    @ToString.Exclude
    List<Student> students;

    @OneToMany(mappedBy = "university" , fetch = FetchType.LAZY , cascade = CascadeType.ALL)
    @JsonIgnore
    @ToString.Exclude
    List<Teacher> teachers;
}

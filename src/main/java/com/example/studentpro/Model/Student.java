package com.example.studentpro.Model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Student implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String surname;
    String email;
    String password;

    @ManyToOne
    @JoinColumn(name = "university_id")
    University university;

    @ManyToMany
    @JoinTable(name = "students_teacher" ,
    joinColumns = @JoinColumn(name = "student_id" , referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "teacher_id" , referencedColumnName = "id"))
    List<Teacher> teachers;

}
